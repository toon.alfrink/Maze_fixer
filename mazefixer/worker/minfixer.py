from sqlalchemy import select, update
import json
from queue import PriorityQueue
from time import sleep

from mazefixer.web.db import maze_table, execute

# todo: type annotations everywhere

def run():
    # IO and pure functions are strictly separated at the top level, to maximize unit testing coverage
    while True: # todo: make this run on a database event instead of polling
        queued_mazes = execute(select_queued_maze_query())
        if (maze := queued_mazes.first()):
            solution_query = insert_maze_solution_query(maze)
            execute(solution_query)
        sleep(4)


def select_queued_maze_query():
    return select(maze_table).where(maze_table.c.min_solution_status == 'pending')

def insert_maze_solution_query(maze):

    status, solution = solve_maze(maze.n_rows, maze.n_columns, set(map(tuple,json.loads(maze.walls))), maze.entrance_row, maze.entrance_column)
    return update(maze_table).where(maze_table.c.id == maze.id).values(
        min_solution=json.dumps(solution),
        min_solution_status=status
    )


class Node:
    def __init__(self, column, row, parent = None):
        self.column = column
        self.row = row
        self.parent = parent
        self.pathlength = 0
        if parent:
            self.pathlength = parent.pathlength + 1

    def __lt__(self, other):
        return self.pathlength < other.pathlength

    def __eq__(self, other):
        return self.column == other.column and self.row == other.row

    def __str__(self):
        return f"Node ({self.column},{self.row})"

def _get_neighbors(parent, n_columns, n_rows, walls, open_nodes, closed_nodes):
    candidates = [
        Node(parent.column, parent.row - 1, parent),
        Node(parent.column, parent.row + 1, parent),
        Node(parent.column - 1, parent.row, parent),
        Node(parent.column + 1, parent.row, parent)
    ]
    valid = lambda c: all((
        c.column >= 0,
        c.column < n_columns,
        c.row >= 0,
        c.row < n_rows,
        (c.column, c.row) not in walls,
        c not in open_nodes.queue,
        c not in closed_nodes
    ))
    return filter(valid, candidates)


def solve_maze(n_columns, n_rows, walls, entrance_column, entrance_row):
    """we need to check if mazes are valid by checking for multiple exits. As long as there is at least one
    unreachable exit, this can only be verified by doing a full search. Hence we don't benefit from A* over Dijkstra
    since we need to check all paths anyway.
    Alternatively one could attempt an A* search from every exit node to the entrance node, which could be faster in
    some cases because not all paths would have to be searched, but I don't have time to verify that
    """

    open_nodes = PriorityQueue()
    open_nodes.put(Node(entrance_column, entrance_row))
    closed_nodes = []
    while not open_nodes.empty():
        node = open_nodes.get()
        closed_nodes.append(node)
        for neighbor in _get_neighbors(node, n_columns, n_rows, walls, open_nodes, closed_nodes):
            open_nodes.put(neighbor)
    exitnodes = [node for node in closed_nodes if node.row == n_rows - 1]
    if len(exitnodes) == 0:
        return "error: unsolveable", []
    elif len(exitnodes) > 1:
        return "error: multiple exits", []
    else:
        path = [exitnodes[0]]
        while p := path[-1].parent:
            path.append(p)
        return "solved", [(node.column, node.row) for node in reversed(path)]

if __name__ == "__main__":
    run()