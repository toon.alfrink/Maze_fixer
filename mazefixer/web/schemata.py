from pydantic import constr, BaseModel, Field
from enum import Enum

GridSize = constr(regex=r"[1-9][0-9]*x[1-9][0-9]*")
Cell = constr(regex=r'[A-Z]+[0-9]+')
CellSet = set[Cell]


class SolutionStatus(str, Enum):
    solved = 'solved'
    pending = 'pending'
    error_toomanyexits = 'error: multiple exits'
    error_unsolveable = 'error: unsolveable'


class NewMaze(BaseModel):
    gridSize: GridSize = Field(..., example="8x8")
    walls: CellSet = Field(...,
                           example=["C0", "G0", "A1", "C1", "E1", "G1", "C2", "E2", "B3", "C3", "E3", "F3", "G3", "B4",
                                    "E4", "B5", "D5",
                                    "E5", "G5", "H5", "B6", "D6", "G6", "B7"])
    entrance: Cell = Field(..., example="A0")


class UserIn(BaseModel):
    username:str = Field(..., min_length=1, example="demo")
    password:str = Field(..., min_length=1, example="12345")

class UserOut(BaseModel):
    id: int
    username:str