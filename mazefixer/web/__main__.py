from fastapi import FastAPI

app = FastAPI(
    title='Maze Fixer',
    contact = {
        'name' : "Toon Alfrink",
        'email' : "toon.alfrink@gmail.com"
    },
    docs_url="/"
)

# todo: make this automatically discovered
from .endpoints.getmazes import router as getmazes_router
app.include_router(getmazes_router)

from .endpoints.postmaze import router as postmaze_router
app.include_router(postmaze_router)

from .endpoints.getsolution import router as getsolution_router
app.include_router(getsolution_router)

from .endpoints.loginuser import router as loginuser_router
app.include_router(loginuser_router)

from .endpoints.registeruser import router as registeruser_router
app.include_router(registeruser_router)



