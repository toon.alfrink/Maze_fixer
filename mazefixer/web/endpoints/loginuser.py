from sqlalchemy import update, select
from datetime import datetime
import string
from random import choices
from fastapi import APIRouter, HTTPException
from pydantic import constr

from ..db import execute, user_table
from ..schemata import UserIn

router = APIRouter()


@router.post("/login", response_model=constr(min_length=10, max_length=16), tags=["Authentication"])
def login(user: UserIn):
    """Logs in a user. Returns a bearer token that will be valid for 30 days"""
    query = select(user_table).where(user_table.c.username == user.username)
    user_result = execute(query).one_or_none()
    if not user_result:
        raise HTTPException(status_code=404, detail="user not found")
        # I'm aware that most services don't tell this much to the user for
        # security reasons, but I feel that usability is more important
    if not user.password == user_result.password:
        raise HTTPException(status_code=401, detail="incorrect password")

    values, token = login_user_query_values(user_result.session_token, user_result.last_login)
    query = update(user_table).where(
        user_table.c.username == user_result.username
    ).values(**values)

    execute(query)
    return token


def _session_expired(last_login):
    if (datetime.now() - last_login).days > 30:
        return True


def login_user_query_values(session_token, last_login):
    token = ''.join(choices(string.ascii_uppercase + string.digits, k=16))
    # todo: generate a token that is actually secure
    if session_token and not _session_expired(last_login):
        token = session_token
    return {
               'session_token': token,
               'last_login': datetime.now()
           }, token


# todo: tests