from fastapi import APIRouter, Depends
from sqlalchemy import insert
import string
import json

from mazefixer.web.authentication import get_user
from mazefixer.web.db import maze_table, execute
from mazefixer.web.schemata import NewMaze

router = APIRouter()

@router.post("/maze", response_model=int, status_code=201)
def create_maze(maze:NewMaze, user = Depends(get_user)):
    """Creates a maze with the given parameters. Returns the id of the new maze"""
    values = _create_maze_query_values(maze.entrance, maze.gridSize, maze.walls, user.id)
    query = insert(maze_table).values(**values)
    id = execute(query).inserted_primary_key[0]
    return id

def _create_maze_query_values(entrance, gridSize, walls, userid):
    n_rows, n_columns = map(int, gridSize.split("x"))
    _cell_to_dbformat = lambda apiformat: (string.ascii_uppercase.index(apiformat[0]), int(apiformat[1]))
    entrance_column, entrance_row = _cell_to_dbformat(entrance)
    return {
        'n_rows': n_rows,
        'n_columns': n_columns,
        'walls': json.dumps(list(map(_cell_to_dbformat, walls))),
        'entrance_column': entrance_column,
        'entrance_row': entrance_row,
        'owner': userid,
        'min_solution_status': 'pending',
        'max_solution_status': 'pending',
    }

# todo: tests